import type { MovieDetails, MovieList } from '$lib/types.js';
import * as api from '$lib/api.js';

export async function load({ fetch }) {
	const trendingData = (await api.get(fetch, 'trending/movie/day')) as MovieList;

	const featured = trendingData.results[0];
	const featuredData = (await api.get(fetch, `movie/${featured.id}`, {
		append_to_response: 'images'
	})) as MovieDetails;

	return {
		trendingData,
		featuredData
	};
}
