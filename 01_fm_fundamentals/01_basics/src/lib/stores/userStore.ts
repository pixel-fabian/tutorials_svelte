import { writable } from "svelte/store";

export function createUsersStore() {
  const url = "https://jsonplaceholder.typicode.com/users";
  const loading = writable(false);
  const error = writable(false);
  const data = writable({});

  async function getUsers() {
    loading.set(true);
    error.set(false);
    try {
      const response = await fetch(url);
      data.set(await response.json());
    } catch (e) {
      error.set(e);
    }
    loading.set(false);
  }
  getUsers();

  return [data, loading, error, getUsers];
}
