type User = {
	id: number;
	name: string;
	username: string;
	email: string;
};

export async function load({ params }) {
	return await getUser(params.slug);
}

async function getUser(id: string) {
	const response = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
	const user = (await response.json()) as User;

	return {
		success: true,
		user: user
	};
}
