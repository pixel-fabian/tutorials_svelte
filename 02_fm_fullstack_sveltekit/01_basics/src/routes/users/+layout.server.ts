type User = {
	id: number;
	name: string;
	username: string;
	email: string;
};

export async function load() {
	return await getUsers();
}

async function getUsers() {
	const response = await fetch('https://jsonplaceholder.typicode.com/users');
	const users = (await response.json()) as User[];

	return {
		success: true,
		users: users
	};
}
