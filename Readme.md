# Svelte Tutorials

Learning about Svelte from:

1. [Frontendmasters: Svelte 3+ Fundamentals](https://frontendmasters.com/courses/svelte-v2)
2. [Frontendmasters: Fullstack Svelte with SvelteKit](https://frontendmasters.com/courses/sveltekit/)

## Links

- [svelte.dev](https://svelte.dev/)
- [learn.svelte.dev](https://learn.svelte.dev/tutorial/welcome-to-svelte)